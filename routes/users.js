/**
 * Created by Dmitri on 25/02/2017.
 */

var express = require('express');
var router = express.Router();
var validator = require("../dataProcess/validator");
var crypt = require("../dataProcess/crypt");
var qBuilder = require("../database/client");
var dataSecure = require("../dataProcess/dataSecure");
const STORE_CLIENT_DATA_PATH = "/store";
const LIST_CLIENTS_PATH = "/list";

router.use(function(req, res, next) {
    var err;
    console.log("request.path: " + req.path);
    console.log("isJsonOk: " + validator.isJsonOk(req.body));

    if(req.path === STORE_CLIENT_DATA_PATH){
        err = validator.checkClientData(req.body);
        if(!err){
            req.body.phone = crypt.encrypt(req.body.phone);
        }
    }

    if(req.path === LIST_CLIENTS_PATH){
        err = validator.checkClientFilter(req.body);
        if(!err && req.body.phone)
            req.body.phone = crypt.encrypt(req.body.phone);
    }
    next(err);
});

router.use(function(err, req, res, next) {
    console.error(err);
    res.status(400).send(err);
});

router.post(LIST_CLIENTS_PATH, function (req, res, next) {
    qBuilder.select(req.body, function(err,response) {
        if(err)
            res.status(500).json(err).end();
        else{
            response = dataSecure.decryptPhoneNumbers(response);
            response = dataSecure.hidePhoneNumbers(response, 4);
            res.status(200).json(response).end();
        }
    });
});

router.post(STORE_CLIENT_DATA_PATH, function (req, res) {

    qBuilder.insert(req.body, function(err,response) {
        if (err) {
            res.status(500).json(err).end();
        }
        else {
            console.log(response);
            res.status(200).json(response).end();
        }
    });
});

module.exports = router;