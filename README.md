# README #

My jar test task.

### Setup ###

* Checkout repository,
* Configure server (config.js in root folder) and database (database/config.json):
    Data base config includes working table structure that can be changed, BUT fields 'id(auto increment primary key)',
    'phone' and  'email' are hardcoded and should not be added to configuration.
* navigate to root directory
* run 'npm install'
* run 'node setup.js'. NB! setup.js will delete table with name given in config file.
* run 'node server.js' to launch listener.

### Usage ###

* Path "/store" used to store new client data. Client data has to be in json format {fieldName, value}...
Only one client can be added within one request;

* Path "/list" used to retrieve client data using search criteria in json format {fieldName, searchValue};

### Author ###

dmitri.astreiko@gmail.com
