/**
 * Created by Dmitri on 27/02/2017.
 */
var crypto = require('crypto');
var algorithm = 'aes-256-ctr';
var password = 'dfg#VbcA@';

var encrypt = function (text){
    var cipher = crypto.createCipher(algorithm,password)
    var crypted = cipher.update(text,'utf8','hex')
    crypted += cipher.final('hex');
    return crypted;
};

var decrypt = function (text){
    var decipher = crypto.createDecipher(algorithm,password)
    var dec = decipher.update(text,'hex','utf8')
    dec += decipher.final('utf8');
    return dec;
};

module.exports.encrypt = encrypt;
module.exports.decrypt = decrypt;