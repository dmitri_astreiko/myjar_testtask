/**
 * Created by Dmitri on 21/02/2017.
 */
const LOCALE = 'en-GB';
var validator = require('validator');

var emailRegExp = /^[A-Za-z]\w+[\w.]\w+@[a-z]\w+\.[a-z]+/;
var err = "";



var isPhoneOk = function (input){
    return validator.isMobilePhone(input, LOCALE);
};

var isEmailOk = function (input){
    var OK = emailRegExp.exec(input);
    return(OK !== null);
};

var isJsonOk = function (input) {
    return (validator.isJSON(JSON.stringify(input)) && (Object.keys(input).length > 0));
};

var checkClientData = function (data) {
    var err = "";

    if(data.phone){
        if(!isPhoneOk(data.phone))
            err += "Invalid phone number.\n";
    }
    else{
        err += "Phone number is missing.\n";
    }

    if(data.email){
        if(!isEmailOk(data.email))
            err += "Invalid email.\n";
    }
    else{
        err += "Email is missing.\n";
    }
    if (err.length > 0) return err;
};

var checkClientFilter = function (data) {
    var err = "";
    if(!isJsonOk(data))
        return("Invalid JSON data");

    if (err.length > 0) return err;
}

module.exports.isPhoneOk = isPhoneOk;
module.exports.isEmailOk = isEmailOk;
module.exports.isJsonOk = isJsonOk;
module.exports.checkClientData = checkClientData;
module.exports.checkClientFilter = checkClientFilter;
