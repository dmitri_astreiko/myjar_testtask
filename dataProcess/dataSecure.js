/**
 * Created by Dmitri on 27/02/2017.
 */
var crypt = require("./crypt");
var hidePhoneNumbers = function (data, digitsToShow){
    //TODO implement different amount of digits to show
    for(var i = 0; i < data.length; i++){
        if(data[i].phone){
            data[i].phone = "***" + data[i].phone.substr(data[i].phone.length - 4);
        }
    }
    return data;
};

var decryptPhoneNumbers = function (data) {
    for(var i = 0; i < data.length; i++){
        if(data[i].phone){
            data[i].phone = crypt.decrypt(data[i].phone);
        }
    }
    return data;
};

module.exports.hidePhoneNumbers = hidePhoneNumbers;
module.exports.decryptPhoneNumbers = decryptPhoneNumbers;