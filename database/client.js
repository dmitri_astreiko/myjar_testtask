/**
 * Created by Dmitri on 21/02/2017.
 *
 * Manual reference for query builder configuration and filter format:
 *  https://www.npmjs.com/package/node-querybuilder#database-drivers
 *
 */

var config = require('./config.json');
var qBuilder = require('node-querybuilder').
    QueryBuilder(config.settings, config.dbType, config.connectionType);

var select = function(filter, cb) {
    //qBuilder.select('id, firstName, lastName, ' + Object.keys(filter)[0])
    qBuilder.select('*')
        .where(filter)
        .get(config.tableName, cb);
};

var insert = function(data, cb) {
    qBuilder.insert(config.tableName, data, cb);
};

var createTable = function () {
    qBuilder.query("DROP TABLE if exists " + config.tableName, function(err, resp) {
        if (err) {
            console.error(err);
        }
        else {
            qBuilder.query(getCreateTableQuery(), function(err, resp) {
                if (err) {
                    console.error(err);
                }
                else {
                    console.log("New table created successfully.\nDB response:\n" + JSON.stringify(resp));
                    qBuilder.disconnect();
                }
            });
        }
    });
};

var getCreateTableQuery = function () {
    var query = "CREATE TABLE " + config.tableName + " (";

    query += "id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,";
    query += "email VARCHAR(50) NOT NULL,";
    query += "phone VARCHAR(256) NOT NULL,";

    for(var key in config.structure){
        if (config.structure.hasOwnProperty(key)) {
            query += key + " " + config.structure[key] + ",";
        }
    }
    query = query.substring(0, query.length - 1) + ")";
    return query;
};

module.exports.select = select;
module.exports.insert = insert;
module.exports.createTable = createTable;