var express = require('express');        // call express
var bodyParser = require('body-parser');
var app = express();                 // define our app using express
var port = require("./config.js").server.port;
var users = require("./routes/users.js");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/users', users);
app.listen(port, function () {
    console.log('Server is listening on port ' + port);
});
